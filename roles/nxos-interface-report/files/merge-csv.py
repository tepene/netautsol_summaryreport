#!/usr/bin/env python3
import argparse

import pandas


def dataframe_merge(left_df, right_df, left_on, right_on):
    """ Merge two pandas dataframes. All rows of the left dataframe are kept.

    Args:
        left_df : (obj) Left pandas dataframe
        right_df : (obj) Right pandas dataframe
        left_on : (str) Column of the left dataframe to merge on
        right_on : (str) Column of the right dataframe to merge on

    Returns:
        dataframe: Merged pandas dataframe

    """
    df1 = left_df.astype(object)
    df2 = right_df.set_index(right_on)
    dataframe = pandas.merge(df1, df2, how="left", left_on=left_on, right_on=right_on)
    return dataframe


# Script Input Arguments
parser = argparse.ArgumentParser(description="Merge CSV")
parser.add_argument(
    "-l", "--left", required=True, default=None, type=str, help="Left input file",
)
parser.add_argument(
    "-lo",
    "--left_on",
    required=True,
    default=None,
    type=str,
    help="Left column to merge on",
)
parser.add_argument(
    "-r", "--right", required=True, default=None, type=str, help="Right input file",
)
parser.add_argument(
    "-ro",
    "--right_on",
    required=True,
    default=None,
    type=str,
    help="Right column to merge on",
)
parser.add_argument(
    "-n",
    "--name",
    required=True,
    default=None,
    type=str,
    help="Name of new CSV file. Can also be a file path with file name",
)

args = parser.parse_args()


if __name__ == "__main__":
    df1 = pandas.read_csv(args.left, na_values=["-", "--"])
    df2 = pandas.read_csv(args.right, na_values=["-", "--"])
    df = dataframe_merge(
        left_df=df1, right_df=df2, left_on=args.left_on, right_on=args.right_on
    )
    df.to_csv(path_or_buf=args.name, index=False, sep=",", encoding="utf-8")
