#!/usr/bin/env python3
import argparse

import pandas


def dataframe_from_json(data, index_name):
    """ Create pandas dataframe from JSON data

    Args:
        data : (str) Valid JSON string or path to JSON file

    Returns:
        dataframe: Pandas dataframe

    """
    dataframe = pandas.read_json(path_or_buf=data, orient="index")
    dataframe.index.name = index_name
    return dataframe


def dataframe_to_csv(dataframe, view, file):
    """ Create CSV from pandas dataframe

    Args:
        dataframe : (obj) Pandas dataframe
        file : (str) File name. Can include file path

    """
    csv_encoding = "utf-8"
    csv_delimiter = ","
    if view is None:
        dataframe.to_csv(path_or_buf=file, sep=csv_delimiter, encoding=csv_encoding)
    else:
        dataframe_view = dataframe[view]
        dataframe_view.to_csv(
            path_or_buf=file, sep=csv_delimiter, encoding=csv_encoding
        )


# Script Input Arguments
parser = argparse.ArgumentParser(description="JSON to CSV converter")

parser.add_argument(
    "-i", "--index", required=True, default=None, type=str, help="Dataframe index name",
)
parser.add_argument(
    "-j",
    "--json",
    required=True,
    default=None,
    type=str,
    help="JSON file name. Can also be a file path with file name",
)
parser.add_argument(
    "-v",
    "--view",
    required=False,
    default=None,
    nargs="+",
    help="Columns of the dataframe you want to have in your CSV",
)
parser.add_argument(
    "-n",
    "--name",
    required=True,
    default=None,
    type=str,
    help="CSV file name. Can also be a file path with file name",
)

args = parser.parse_args()

# JSON to CSV convertion
if __name__ == "__main__":
    data = dataframe_from_json(data=args.json, index_name=args.index)
    dataframe_to_csv(dataframe=data, view=args.view, file=args.name)
