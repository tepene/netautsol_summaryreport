# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.0] - 2020-02-28

### Added

- Changelog

### Fixed

- Parsed facts now get saved to json first before further computing. Fixes issue with large argument list when invoking json2csv.py
- Workaround for libselinux-python issue on RedHat 7.7 until python3-libselinux package is available
