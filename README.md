# NetAutSol_SummaryReport

Ansible summary report for [Building Network Automation Course](https://www.ipspace.net/NetAutSol/Start)

## Playbook - nxos-interface-report.yml

The playbook will generate a interface configurtion report of a Cisco Nexus Switch in NXOS mode.

The report will create a CSV file from the following "show" commands:

- show interface brief
- show interface description
- show interface switchport
- show port-channel summary
- show vpc

The report will be saved under **./data/reports/nxos-interface-report/{{ inventory_hostname }}.csv**

## Prerequisites

Besides a running ansible instance you'll have to make sure that the following Python packages are installed on your ansible host.

- pyats
- genie
- pandas
- paramiko

In addition to the Python packages the code also depends on the following ansible-role:

- [ansible-pyats](https://github.com/CiscoDevNet/ansible-pyats)

## Assumptions

To run the playbook successfully you need Python 3.6 or above. The Python interpreter path is set to **/opt/ansible/bin/python**. Have a look at the [ansible.cfg](ansible.cfg) file and change it to your setup accordingly.

## Usage

To playbook can be run with the following command:

```bash
ansible-playbook nxos-interface-report.yml
```

You will then be asked for a username and password which will be used to connect to the switches

```bash
Enter username which will connect to network devices: ansible
Enter your password:
```

For debug purposes the playbook can also be run with the **debug** tag.

```bash
ansible-playbook nxos-interface-report.yml --tags debug
```

This will save the full json output of the pyats show commands under **./data/nxos-interface-report/debug/{{ inventory_hostname }}**

## License

This project is licensed under the BSD-3-Clause License - see the [LICENSE](LICENSE) file for details.
